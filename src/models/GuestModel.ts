import { JsonAccess } from "../data/JsonAccess";

import { Guest } from "../types/Guest";
export class GuestModel extends JsonAccess {

    private countAppearances(list: Array<Guest>, attribute: string) {
        return list.reduce(function (counter: any, item: any) {
            const attr: string = item[attribute];
            counter[attr] = counter.hasOwnProperty(attr) ? counter[attr] + 1 : 1;
            return counter;
        }, {});
    }

    private buildSortedArray(reducedObj: any) {
        const list: any[] = [];
        const keys = Object.keys(reducedObj);
        return keys.map((key, index) => {
            const element = {} as any;
            element.key = keys[index];
            element.value = reducedObj[key];
            return element;
        }).sort((a, b) => {
            if (a.value > b.value) return -1;
            if (a.value < b.value) return 1;
            return 0;
        });
    }

    public getGuestLocations() {
        return this.getGuests().then((guests: Guest[]) => {
            return guests;
        });
    }

    public getGuestsTotalByBeatle() {
        return this.getGuests().then((guests: Guest[]) => {
            return this.buildSortedArray( this.countAppearances(guests, "guest_of"));
        });
    }

    public getGuestsByLocations() {
        return this.getGuests().then((guests: Guest[]) => {
            return this.buildSortedArray( this.countAppearances(guests, "location"));
        });
    }

    public getAlbumRanking() {
        return this.getGuests().then((guests: Guest[]) => {
            return this.buildSortedArray(this.countAppearances(guests, "favourite_album"));
        });
    }
  }