import * as guests  from "../controllers/guests";
import {} from 'jest';
import * as supertest from "supertest";
const request = supertest("http://localhost:8000");


jest.mock('../models/GuestModel');
const GuestModel = require('../models/GuestModel');
const myMock = jest.fn();

GuestModel.getGuestsByLocations = jest.fn().mockImplementation(function () {
  return Promise.resolve({});
});

GuestModel.getAlbumRanking = jest.fn().mockImplementation(function () {
  return Promise.resolve({});
});

GuestModel.getGuestsTotalByBeatle = jest.fn().mockImplementation(function () {
  return Promise.resolve({});
});


describe("Guest Controller", () => {
  it("should return 200", (done) => {
    request.get("/")
      .expect(200, done);
  });

  it("should return 404", (done) => {
    request.get("/guestswrong")
      .expect(404, done);
  });

  it("should render guests view", (done) => {
    request.get("/").then(response => {
      expect(response).toBe('')
      done();
    }).catch(err => {
      expect(err).toBe('9');
      done();
    })

  });
});