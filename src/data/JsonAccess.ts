import { Access } from "./Access";
import fs = require("fs");
export class JsonAccess implements Access {
    getGuests() {
        return new Promise(function(resolve: any, reject: any) {
            fs.readFile( __dirname + "/json/Guests.json", "utf8", function (err: any, data: any) {
                resolve(JSON.parse(data));
            });
        });
    }
}