
export class Guest {
    
    name: string;
    location: string;
    guest_of: string;
    favourite_album: string;

    constructor(name: string, location: string, guest_of: string, favourite_album: string);
    clone(): Guest;
}