import { Request, Response } from "express";
import { GuestModel } from "../models/GuestModel";
import { Guest } from "../types/Guest";
/**
 * GET /
 * Guests page.
 */
export let index = (req: Request, res: Response) => {
    const model = new GuestModel();
    const promises = [model.getGuestsByLocations(), model.getGuestsTotalByBeatle(), model.getAlbumRanking()];
    return Promise.all(promises).then((values: any[]) => {
        res.render("guests", {
            locations: values[0],
            beatles: values[1],
            albums: values[2]
        });
    });
};
