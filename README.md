
# Pre-reqs
- Install [Node.js](https://nodejs.org/en/)

# Getting started
- Clone the repository
- Install dependencies
```
cd <project_name>
npm install

- Build and run the project
```
npm start
```
Navigate to `http://localhost:3000`
